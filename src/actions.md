# Actions

Here, you will find examples of actions with different programs.

- [Firewalld](actions/firewalld.md)
- [Iptables](actions/iptables.md)
