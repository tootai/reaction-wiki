# Configs of ppom

- [`server.jsonnet`](./ppom/server.jsonnet.md): more or less the same conf which is on my principal homeserver.
- [`activitywatch.jsonnet`](./ppom/activitywatch.jsonnet.md): a WIP attempt to do a small [activity-watch](https://github.com/ActivityWatch/activitywatch/) clone
- [`example.jsonnet`](https://framagit.org/ppom/reaction/-/blob/main/config/example.jsonnet), which is the upstream example.

{{#include ./ppom/server-jsonnet.md}}
