# Filters

Here, you will find examples of filters for different programs’ logs.

- [Directus](filters/directus.md)
- [Nextcloud](filters/nextcloud.md)
- [Nginx](filters/nginx.md)
- [Slskd](filters/slskd.md)
- [SSH](filters/ssh.md)
- [Traefik](filters/traefik.md)
- [Web crawlers](filters/web-crawlers.md)
- [Web servers common log format](filters/webservers-common-log-format.md)
