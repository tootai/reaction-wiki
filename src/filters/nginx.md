# Nginx

Configuration for the [Nginx](https://nginx.org/) web server.

Nginx most often logs to `/var/log/nginx/access.log`.

The [Common Log Format, used by multiple webservers, is described in another wiki page](webservers-common-log-format.md).

Examples in this wiki use this configuration in nginx's `http { }` block:

```nginx
log_format withhost '$remote_addr - $remote_user [$time_local] $host "$request" $status $bytes_sent "$http_referer" "$http_user_agent"';
access_log /var/log/nginx/access.log withhost;
```

*As a pattern, we'll use ip. See [here](/patterns#ip).*

This permits an easy access to the IP, as it's at the very beginning.
It also permits to have access to the domain name, which is useful when nginx is configured for multiple virtual hosts.

A regex for nginx can look like this:
```jsonnet
@'^<ip> .* "POST /auth/login HTTP/..." 401 '
//          ^    ^                     ^
//     Method    Path        Status Code
```

Or this:
```jsonnet
@'^<ip> .* domain.name "POST /auth/login HTTP/..." 401 '
//         ^            ^    ^                     ^
//         Domain  Method    Path        Status Code
```
*Adjust `domain.name` according to your domain*

Example: 
```jsonnet
{
  streams: {
    nginx: {
      cmd: ['tail', '-n0', '-f', '/var/log/nginx/access.log'],
      filters: {
        directus: {
          regex: [
            @'^<ip> .* directus.domain "POST /auth/login HTTP/..." 401 ',
          ],
          actions: banFor('1h'),
        },
      },
    },
  },
}
```

You can decide that all `401`, *Unauthorized*, and `403`, *Forbidden*, are suspicious, and have a filter for *any* 401 and 403:

Example: 
```jsonnet
{
  streams: {
    nginx: {
      cmd: ['tail', '-n0', '-f', '/var/log/nginx/access.log'],
      filters: {
        all403s: {
          regex: [
            @'^<ip> .* "(POST|GET) /[^ ]* HTTP/..." (401|403) ',
          ],
          retry: 15,
          retryperiod: '5m',
          actions: banFor('1h'),
        },
      },
    },
  },
}
```
