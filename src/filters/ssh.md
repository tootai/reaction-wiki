# SSH

Configuration for the [OpenSSH](https://github.com/openssh/openssh-portable) service.

*As an action, we'll use iptables. See [here](/actions/iptables).*

*As a pattern, we'll use ip. See [here](/patterns#ip).*

```jsonnet
{
  streams: {
    // Ban hosts failing to connect via ssh
    ssh: {
      // Use systemd's `journalctl` to tail logs
      cmd: [' journalctl', '-fn0', '-u', 'sshd.service'],
      filters: {
        failedlogin: {
          regex: [
            // Auth fail
            @'authentication failure;.*rhost=<ip>',
            // Client disconnects during authentication
            @'Connection (reset|closed) by (authenticating|invalid) user .* <ip>',
            // More specific auth fail
            @'Failed password for .* from <ip>',
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h'),
        },
      },
    },
  },
}
```

Depending on the Linux distributions (or other UNIX systems), your OpenSSH logs may vary.

Check yourself what logs are printed by your SSH server!
