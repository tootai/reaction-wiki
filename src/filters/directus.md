# Directus

Configuration for the [Directus](https://directus.io) web service.

Directus doesn't log failed login attempts, so one must read the logs of the reverse proxy
configured in front of Directus.

The HTTP code sent after a failed login is `401`, *Unauthorized*.

The request to authenticate on Directus is a POST on `/auth/login`.

*As a pattern, we'll use ip. See [here](/patterns#ip).*

A regex for nginx can look like this:
```jsonnet
@'^<ip> .* domain.name "POST /auth/login HTTP/..." 401 '
```
- adjust https://domain.name according to your domain.
- if directus is served on a subpath, say `/editor`, then adjust to `POST /editor/auth/login`

Example: 
```jsonnet
{
  streams: {
    nginx: {
      cmd: ['...'], // see ./nginx.md
      filters: {
        directus: {
          regex: [
            @'^<ip> .* directus.domain "POST /auth/login HTTP/..." 401 '
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h'),
        },
      },
    },
  },
}
```
