# Summary

[Introduction](readme.md)

- [Tutorials](tutorials.md)

# Good practices

- [Security](security.md)
- [JSONnet](jsonnet.md)
- [Stream sources](streams.md)

# Examples

- [Patterns](patterns.md)
- [Filters](filters.md)
  - [Directus](filters/directus.md)
  - [Nextcloud](filters/nextcloud.md)
  - [Nginx](filters/nginx.md)
  - [Slskd](filters/slskd.md)
  - [SSH](filters/ssh.md)
  - [Traefik](filters/traefik.md)
  - [Web crawlers](filters/web-crawlers.md)
  - [Web servers common log format](filters/webservers-common-log-format.md)
- [Actions](actions.md)
  - [Firewalld](actions/firewalld.md)
  - [Iptables](actions/iptables.md)
- [Configurations](configurations.md)
  - [Configs of ppom](configurations/ppom.md)
    - [`server.jsonnet`](configurations/ppom/server.jsonnet.md)
    - [`activitywatch.jsonnet`](configurations/ppom/activitywatch.jsonnet.md)
