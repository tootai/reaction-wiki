# reaction

Welcome on reaction's Wiki!

See it live on <https://reaction.ppom.me>!

❤️ Please enhance this wiki with your own discoveries! ❤️

## mdBook

The wiki is statically build with [mdBook](https://rust-lang.github.io/mdBook/).

Here’s some pointers:

- you don’t need mdBook to modify the wiki: add a page in `src` directory, put a link to the page in `src/SUMMARY.md` (very important!), commit your changes, push them and create a merge request
- if you want to use mdBook, you can get an executable binary from [GitHub Releases page](https://github.com/rust-lang/mdBook/releases)
- from the top of the repository, launch `mdbook serve --open` and you will get a preview of the wiki
- create a link in `src/SUMMARY.md`: if it not exists, mdBook will create the file for you
- you can build the wiki with `mdbook build`
- see [the official site](https://rust-lang.github.io/mdBook/) to learn more about mdBook capacities
